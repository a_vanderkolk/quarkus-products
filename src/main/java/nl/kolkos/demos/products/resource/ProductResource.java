package nl.kolkos.demos.products.resource;

import io.quarkus.hibernate.orm.rest.data.panache.PanacheRepositoryResource;
import io.quarkus.rest.data.panache.ResourceProperties;
import nl.kolkos.demos.products.entity.Product;
import nl.kolkos.demos.products.repository.ProductRepository;

@ResourceProperties(hal = true)
public interface ProductResource extends PanacheRepositoryResource<ProductRepository, Product, Long> {

}
