package nl.kolkos.demos.products.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class Product {
    @Id
    @GeneratedValue
    private long id;

    private String name;

    private String description;

    private double price;

}
