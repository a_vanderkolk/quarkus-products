package nl.kolkos.demos.products.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import nl.kolkos.demos.products.entity.Product;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ProductRepository implements PanacheRepository<Product> {

}
